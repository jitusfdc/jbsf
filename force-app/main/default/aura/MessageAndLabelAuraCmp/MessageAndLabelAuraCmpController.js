({
	doInit : function(component, event, helper) {
		var getMessageAction = component.get("c.getMessageAndLabel");
        getMessageAction.setCallback(this, function(response) {
            var messageObj = JSON.stringify(response.getReturnValue()); 
            console.log(':'+messageObj);
            component.set('v.last_name_error',response.getReturnValue()["Last name error"].Value__c);
            component.set('v.submit',response.getReturnValue()["Submit"].Value__c);
            component.set('v.cancel',response.getReturnValue()["Cancel"].Value__c);
        });
        $A.enqueueAction(getMessageAction);
	}
})