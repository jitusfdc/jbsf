import { LightningElement, wire, track} from 'lwc';
import getMessageAndLabel from '@salesforce/apex/MessageAndLabelAuraCmpController.getMessageAndLabel';
export default class MessageAndLabelCmp extends LightningElement {
@track submit;
@track emailFormatErrMsg;
    @wire(getMessageAndLabel)
    wiredMessageAndLabelCs({ error, data }) {
        if (data) {
            this.error = undefined;
            this.test = data;
            this.emailFormatErrMsg = data['Invalid email format error'].Value__c;
            this.submit = data['Submit'].Value__c;
        } else if (error) {
            this.error = error;
        }
    }
}