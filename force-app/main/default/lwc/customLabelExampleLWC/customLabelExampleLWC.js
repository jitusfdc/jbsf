import { LightningElement } from 'lwc';
// importing Custom Label
import CustomerAccRecordType from '@salesforce/label/c.Customer_Account';
import RegdFormHeader from '@salesforce/label/c.Customer_Registration_Form';
export default class CustomLabelExampleLWC extends LightningElement {
    label = {
        CustomerAccRecordType,
        RegdFormHeader
    };
}