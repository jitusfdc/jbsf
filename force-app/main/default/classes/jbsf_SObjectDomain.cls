public virtual with sharing class jbsf_SObjectDomain  {
    public List<SObject> Records { get; private set;}
    @TestVisible
    protected Map<Id, SObject> ExistingRecords{
        get
        {
            system.debug('@@@@@@@@Inside ExistingRecords---->'+ExistingRecords);
            if (ExistingRecords == null)
            {
                system.debug(' if ExistingRecords = null');
                if (System.Test.isRunningTest() & Test.Database.hasRecords())
                {
                    system.debug('System.Test.isRunningTest()---->'+System.Test.isRunningTest());
                    system.debug('Test.Database.hasRecords()---->'+Test.Database.hasRecords());
                    // If in test context and records are in the mock database use those instead of Trigger.oldMap
                    ExistingRecords = Test.Database.oldRecords;
                }
                else
                {
                    system.debug('Else....');
                    ExistingRecords = Trigger.oldMap;
                }
            }
            system.debug('ExistingRecords---->'+ExistingRecords);
            return ExistingRecords;
        }
        private set;
    }
    public Schema.DescribeSObjectResult SObjectDescribe {get; private set;}
    public Configuration Configuration {get; private set;}  
    public static ErrorFactory Errors  {get; private set;}
    public static TestFactory Test  {get; private set;}
    private static Map<Type, List<jbsf_SObjectDomain>> TriggerStateByClass; 
    private static Map<Type, TriggerEvent> TriggerEventByClass;
    static{
        Errors = new ErrorFactory();
        Test = new TestFactory();
        TriggerStateByClass = new Map<Type, List<jbsf_SObjectDomain>>();
        TriggerEventByClass = new Map<Type, TriggerEvent>();
        system.debug('Errors---->'+Errors);
        system.debug('Test---->'+Test);
        system.debug('TriggerStateByClass---->'+TriggerStateByClass);
        system.debug('TriggerEventByClass---->'+TriggerEventByClass);
    }
    public jbsf_SObjectDomain(List<SObject> sObjectList){
        this(sObjectList, sObjectList.getSObjectType());
        system.debug('jbsf_SObjectDomain----');
        system.debug('sObjectList----'+sObjectList);
        system.debug('sObjectList.getSObjectType()----'+sObjectList.getSObjectType());
    }
    public jbsf_SObjectDomain(List<SObject> sObjectList, SObjectType sObjectType) {
        Records = sObjectList.clone(); 
        SObjectDescribe = sObjectType.getDescribe();
        Configuration = new Configuration(); 
        system.debug('jbsf_SObjectDomain---');
        system.debug('Records---'+Records);
        system.debug('SObjectDescribe---'+SObjectDescribe);
        system.debug('Configuration---'+Configuration);
    }
    public virtual void onApplyDefaults() { }
    public virtual void onValidate() {  }
    public virtual void onValidate(Map<Id,SObject> existingRecords) { }
    public virtual void onBeforeInsert() { }
    public virtual void onBeforeUpdate(Map<Id,SObject> existingRecords) { }
    public virtual void onBeforeDelete() { }
    public virtual void onAfterInsert() { }
    public virtual void onAfterUpdate(Map<Id,SObject> existingRecords) { }
    public virtual void onAfterDelete() { } 
    public virtual void onAfterUndelete() { }
    public virtual void handleBeforeInsert() { 
        system.debug('handleBeforeInsert---');
        onApplyDefaults(); 
        onBeforeInsert();
    }
    public virtual void handleBeforeUpdate(Map<Id,SObject> existingRecords) {
        system.debug('handleBeforeUpdate---'+existingRecords);
        onBeforeUpdate(existingRecords);
    }
    public virtual void handleBeforeDelete() {
        system.debug('handleBeforeDelete---');
        onBeforeDelete();
    }
    public virtual void handleAfterInsert(){
        system.debug('handleBeforeDelete---');
        if(Configuration.EnforcingTriggerCRUDSecurity && !SObjectDescribe.isCreateable()) 
           throw new DomainException('Permission to create an ' + SObjectDescribe.getName() + ' denied.');
        onValidate();
        onAfterInsert(); 
    }
    public virtual void handleAfterUpdate(Map<Id,SObject> existingRecords) {     
        system.debug('handleAfterUpdate---');
        system.debug('existingRecords---'+existingRecords);
        system.debug('Configuration.EnforcingTriggerCRUDSecurity--'+Configuration.EnforcingTriggerCRUDSecurity);
        system.debug('SObjectDescribe.isUpdateable()---'+SObjectDescribe.isUpdateable());
        system.debug('Configuration.OldOnUpdateValidateBehaviour---'+Configuration.OldOnUpdateValidateBehaviour);
        if(Configuration.EnforcingTriggerCRUDSecurity && !SObjectDescribe.isUpdateable())                       
           throw new DomainException('Permission to update an ' + SObjectDescribe.getName() + ' denied.');
        
        if(Configuration.OldOnUpdateValidateBehaviour)
            onValidate();
        onValidate(existingRecords);
        onAfterUpdate(existingRecords); 
    }
    public virtual void handleAfterDelete() {
        system.debug('handleAfterDelete----');
        system.debug('Configuration.EnforcingTriggerCRUDSecurity--'+Configuration.EnforcingTriggerCRUDSecurity);
         system.debug('SObjectDescribe.isUpdateable()---'+SObjectDescribe.isUpdateable());
        if(Configuration.EnforcingTriggerCRUDSecurity && !SObjectDescribe.isDeletable())
           throw new DomainException('Permission to delete an ' + SObjectDescribe.getName() + ' denied.');
           
        onAfterDelete();
    } 
    public virtual void handleAfterUndelete() {
        system.debug('handleAfterUndelete----');
        system.debug('Configuration.EnforcingTriggerCRUDSecurity--'+Configuration.EnforcingTriggerCRUDSecurity);
         system.debug('SObjectDescribe.isUpdateable()---'+SObjectDescribe.isUpdateable());
        if(Configuration.EnforcingTriggerCRUDSecurity && !SObjectDescribe.isCreateable())
           throw new DomainException('Permission to create an ' + SObjectDescribe.getName() + ' denied.');
           
        onAfterUndelete();
    } 
    public SObjectType getSObjectType(){
        system.debug('getSObjectType----');
        return SObjectDescribe.getSObjectType();
    }
    public SObjectType sObjectType() {
        system.debug('sObjectType----');
        return getSObjectType();
    }
    public List<SObject> getRecords(){
        system.debug('sObjectType----'+Records);
        return Records;
    }
    public List<SObject> getChangedRecords(Set<String> fieldNames){
        system.debug('getChangedRecords----'+fieldNames);
        List<SObject> changedRecords = new List<SObject>();
        for (SObject newRecord : Records)
        {
            Id recordId = (Id) newRecord.get('Id');
             system.debug('recordId----'+recordId);
            if (this.ExistingRecords == null || !this.ExistingRecords.containsKey(recordId))
            {
                 system.debug('continue----');
                continue;
            }
            SObject oldRecord = this.ExistingRecords.get(recordId);
            system.debug('oldRecord----'+oldRecord);
            for (String fieldName : fieldNames)
            {
                system.debug('fieldName----'+fieldName);
                if (oldRecord.get(fieldName) != newRecord.get(fieldName))
                {
                    system.debug('break----');
                    changedRecords.add(newRecord);
                    break;  // prevents the records from being added multiple times
                }
            }
        }
        system.debug('changedRecords---'+changedRecords);
        return changedRecords;
    }
    public List<SObject> getChangedRecords(Set<Schema.SObjectField> fieldTokens){
         system.debug('getChangedRecords---');
         system.debug('fieldTokens---'+fieldTokens);
         system.debug('Records---'+Records);
        List<SObject> changedRecords = new List<SObject>();
        for (SObject newRecord : Records)
        {
            system.debug('newRecord---'+newRecord);
            Id recordId = (Id) newRecord.get('Id');
            system.debug('recordId---'+recordId);
            if (this.ExistingRecords == null || !this.ExistingRecords.containsKey(recordId))
            {
                system.debug('continue---');
                continue;
            }
            SObject oldRecord = this.ExistingRecords.get(recordId);
            system.debug('oldRecord---'+oldRecord);
            for (Schema.SObjectField fieldToken : fieldTokens)
            {
                 system.debug('fieldToken---'+fieldToken);
                if (oldRecord.get(fieldToken) != newRecord.get(fieldToken))
                {
                    system.debug('break---');
                    changedRecords.add(newRecord);
                    break;  // prevents the records from being added multiple times
                }
            }
        }
        system.debug('changedRecords---'+changedRecords);
        return changedRecords;
    }
    public interface IConstructable{
        jbsf_SObjectDomain construct(List<SObject> sObjectList);
    }
    public interface IConstructable2 extends IConstructable{
        jbsf_SObjectDomain construct(List<SObject> sObjectList, SObjectType sObjectType);
    }
    public static jbsf_SObjectDomain getTriggerInstance(Type domainClass){
        system.debug('getTriggerInstance---');
        system.debug('domainClass---'+domainClass);
        List<jbsf_SObjectDomain> domains = TriggerStateByClass.get(domainClass);
        system.debug('domains---'+domains);
        system.debug('domains[domains.size()-1]---'+domains[domains.size()-1]);
        if(domains==null || domains.size()==0)
            return null;
        return domains[domains.size()-1];
    }
    public static void triggerHandler(Type domainClass){       
        system.debug('triggerHandler---');
        system.debug('domainClass---'+domainClass);
        if(System.Test.isRunningTest() & Test.Database.hasRecords())
        {
           system.debug('Test.isRunningTest()---'+System.Test.isRunningTest());
            // If in test context and records in the mock database delegate initially to the mock database trigger handler
            Test.Database.testTriggerHandler(domainClass);
        }
        else
        {
            system.debug('Else');
            //Lowe's customization for turning on and off using custom settings
            TriggerSettings__c triggerSetting = TriggerSettings__c.getInstance();
            String settingName = domainClass.getName() + '__c';
            system.debug('triggerSetting---'+triggerSetting);
            system.debug('settingName---'+settingName);
            if (triggerSetting.get(settingName) != null && !(Boolean)triggerSetting.get(settingName)){
                system.debug('getTriggerEvent(domainClass).disableAll()');
                getTriggerEvent(domainClass).disableAll();
            } else {
                system.debug('getTriggerEvent(domainClass).enableAll()');
                getTriggerEvent(domainClass).enableAll();
            }

         system.debug('Process the runtime Apex Trigger context '); 
            triggerHandler(domainClass, 
                Trigger.isBefore, 
                Trigger.isAfter, 
                Trigger.isInsert, 
                Trigger.isUpdate, 
                Trigger.isDelete, 
                Trigger.isUnDelete,
                Trigger.new, 
                Trigger.oldMap);
        }
    }
    
    private static void triggerHandler(Type domainClass, Boolean isBefore, Boolean isAfter, Boolean isInsert, Boolean isUpdate, Boolean isDelete, Boolean isUndelete, List<SObject> newRecords, Map<Id, SObject> oldRecordsMap)
    {   
        system.debug('triggerHandler---');
        // After phase of trigger will reuse prior instance of domain class if ITriggerStateful implemented 
        jbsf_SObjectDomain domainObject = isBefore ? null : popTriggerInstance(domainClass, isDelete ? oldRecordsMap.values() : newRecords);
        system.debug('domainObject---'+domainObject);
        system.debug('domainClass---'+domainClass);
        system.debug('isBefore---'+isBefore);
        system.debug('isAfter---'+isAfter);
        system.debug('isInsert---'+isInsert);
        system.debug('isUpdate---'+isUpdate);
        system.debug('isDelete---'+isDelete);
        system.debug('isUndelete---'+isUndelete);
        system.debug('newRecords---'+newRecords);
        system.debug('oldRecordsMap---'+oldRecordsMap);
        if(domainObject==null)
        {       
            // Construct the domain class constructor class
            String domainClassName = domainClass.getName();
            system.debug('domainClassName---'+domainClassName);
            Type constructableClass = domainClassName.endsWith('Constructor') ? Type.forName(domainClassName) : Type.forName(domainClassName+'.Constructor');
            system.debug('constructableClass---'+constructableClass);
            IConstructable domainConstructor = (IConstructable) constructableClass.newInstance();
            system.debug('domainConstructor---'+domainConstructor);
            
            // Construct the domain class with the approprite record set        
            if(isInsert) domainObject = domainConstructor.construct(newRecords);
            else if(isUpdate) domainObject = domainConstructor.construct(newRecords);
            else if(isDelete) domainObject = domainConstructor.construct(oldRecordsMap.values());
            else if(isUndelete) domainObject = domainConstructor.construct(newRecords);
            
            system.debug('domainObject.Configuration.TriggerStateEnabled---'+domainObject.Configuration.TriggerStateEnabled);
            // Should this instance be reused on the next trigger invocation?
            if(domainObject.Configuration.TriggerStateEnabled)
                // Push this instance onto the stack to be popped during the after phase
                system.debug('Push this instance onto the stack to be popped during the after phase');
                pushTriggerInstance(domainClass, domainObject); 
        }
        
        // has this event been disabled?
        if(!getTriggerEvent(domainClass).isEnabled(isBefore, isAfter, isInsert, isUpdate, isDelete, isUndelete))
        {
            system.debug('has this event been disabled?...return');
            return;
        }

        // Invoke the applicable handler
        if(isBefore)
        {
            system.debug('isBefore----'+isBefore);
            if(isInsert) domainObject.handleBeforeInsert();
            else if(isUpdate) domainObject.handleBeforeUpdate(oldRecordsMap);
            else if(isDelete) domainObject.handleBeforeDelete();
        }
        else
        {
            system.debug('isBefore----'+isBefore);
            if(isInsert) domainObject.handleAfterInsert();
            else if(isUpdate) domainObject.handleAfterUpdate(oldRecordsMap);
            else if(isDelete) domainObject.handleAfterDelete();
            else if(isUndelete) domainObject.handleAfterUndelete();
        }               
    }
    private static void pushTriggerInstance(Type domainClass, jbsf_SObjectDomain domain){
        system.debug('pushTriggerInstance----');
        system.debug('domainClass----'+domainClass);
        system.debug('domain----'+domain);
        List<jbsf_SObjectDomain> domains = TriggerStateByClass.get(domainClass);
         system.debug('domains----'+domains);
        if(domains==null)
            TriggerStateByClass.put(domainClass, domains = new List<jbsf_SObjectDomain>());
        domains.add(domain);
        system.debug('domains---'+domains);        
    }
    private static jbsf_SObjectDomain popTriggerInstance(Type domainClass, List<SObject> records){
        system.debug('popTriggerInstance---');
        system.debug('domainClass---'+domainClass);
        system.debug('records---'+records);
        List<jbsf_SObjectDomain> domains = TriggerStateByClass.get(domainClass);
        system.debug('domains----'+domains);
        if(domains==null || domains.size()==0)
            return null;        
        jbsf_SObjectDomain domain = domains.remove(domains.size()-1);
        domain.Records = records;
        system.debug('records---'+records);
        return domain;
    }    
    public static TriggerEvent getTriggerEvent(Type domainClass) {
        system.debug('getTriggerEvent---');
        system.debug('domainClass---'+domainClass);
        if(!TriggerEventByClass.containsKey(domainClass))
        {
            system.debug('TriggerEventByClass.containsKey(domainClass)');
            TriggerEventByClass.put(domainClass, new TriggerEvent());
        }
        system.debug('TriggerEventByClass.get(domainClass)'+TriggerEventByClass.get(domainClass));
        return TriggerEventByClass.get(domainClass);
    }
    public class TriggerEvent
    {
        
        public boolean BeforeInsertEnabled {get; private set;}
        public boolean BeforeUpdateEnabled {get; private set;}
        public boolean BeforeDeleteEnabled {get; private set;}

        public boolean AfterInsertEnabled {get; private set;}
        public boolean AfterUpdateEnabled {get; private set;}
        public boolean AfterDeleteEnabled {get; private set;}
        public boolean AfterUndeleteEnabled {get; private set;}

        public TriggerEvent()
        {
            system.debug('TriggerEvent...TriggerEvent()');
            this.enableAll();
        }

        // befores
        public TriggerEvent enableBeforeInsert() {BeforeInsertEnabled = true; return this;}
        public TriggerEvent enableBeforeUpdate() {BeforeUpdateEnabled = true; return this;}
        public TriggerEvent enableBeforeDelete() {BeforeDeleteEnabled = true; return this;}

        public TriggerEvent disableBeforeInsert() {BeforeInsertEnabled = false; return this;}
        public TriggerEvent disableBeforeUpdate() {BeforeUpdateEnabled = false; return this;}
        public TriggerEvent disableBeforeDelete() {BeforeDeleteEnabled = false; return this;}
        
        // afters
        public TriggerEvent enableAfterInsert()     {AfterInsertEnabled     = true; return this;}
        public TriggerEvent enableAfterUpdate()     {AfterUpdateEnabled     = true; return this;}
        public TriggerEvent enableAfterDelete()     {AfterDeleteEnabled     = true; return this;}
        public TriggerEvent enableAfterUndelete() {AfterUndeleteEnabled     = true; return this;}

        
        public TriggerEvent disableAfterInsert()    {AfterInsertEnabled     = false; return this;}
        public TriggerEvent disableAfterUpdate()    {AfterUpdateEnabled     = false; return this;}
        public TriggerEvent disableAfterDelete()    {AfterDeleteEnabled     = false; return this;}
        public TriggerEvent disableAfterUndelete(){AfterUndeleteEnabled     = false; return this;}

        public TriggerEvent enableAll()
        {
            system.debug('TriggerEvent...enableAll()');
            return this.enableAllBefore().enableAllAfter();
        }

        public TriggerEvent disableAll()
        {
            system.debug('TriggerEvent...disableAll()');
            return this.disableAllBefore().disableAllAfter();
        }

        public TriggerEvent enableAllBefore()
        {
            system.debug('TriggerEvent...enableAllBefore()');
            return this.enableBeforeInsert().enableBeforeUpdate().enableBeforeDelete();
        }

        public TriggerEvent disableAllBefore()
        {
            system.debug('TriggerEvent...disableAllBefore()');
            return this.disableBeforeInsert().disableBeforeUpdate().disableBeforeDelete();
        }

        public TriggerEvent enableAllAfter()
        {
            system.debug('TriggerEvent...enableAllAfter()');
            return this.enableAfterInsert().enableAfterUpdate().enableAfterDelete().enableAfterUndelete();
        }

        public TriggerEvent disableAllAfter()
        {
            system.debug('TriggerEvent...disableAllAfter()');
            return this.disableAfterInsert().disableAfterUpdate().disableAfterDelete().disableAfterUndelete();
        }

        public boolean isEnabled(Boolean isBefore, Boolean isAfter, Boolean isInsert, Boolean isUpdate, Boolean isDelete, Boolean isUndelete)
        {
            system.debug('isEnabled---()');
            system.debug('isBefore----'+isBefore);
            system.debug('isAfter----'+isAfter);
            system.debug('isInsert----'+isInsert);
            system.debug('isUpdate----'+isUpdate);
            system.debug('isDelete----'+isDelete);
            system.debug('isUndelete----'+isUndelete);
            if(isBefore)
            {
                if(isInsert) return BeforeInsertEnabled;
                else if(isUpdate) return BeforeUpdateEnabled;
                else if(isDelete) return BeforeDeleteEnabled;
            }
            else if(isAfter)
            {
                if(isInsert)        return AfterInsertEnabled;
                else if(isUpdate)   return AfterUpdateEnabled;
                else if(isDelete)   return AfterDeleteEnabled;
                else if(isUndelete) return AfterUndeleteEnabled;
            }
            return true; // shouldnt ever get here!
        }
    }

    public class Configuration{
        public Boolean OldOnUpdateValidateBehaviour {get; private set;}      
        public Boolean EnforcingTriggerCRUDSecurity {get; private set;}
        public Boolean TriggerStateEnabled {get; private set;}
        public Configuration(){
            system.debug('Configuration....');
            
            EnforcingTriggerCRUDSecurity = true; // Default is true for backwards compatability
            system.debug('EnforcingTriggerCRUDSecurity----'+EnforcingTriggerCRUDSecurity);
            TriggerStateEnabled = false;
            system.debug('TriggerStateEnabled----'+TriggerStateEnabled);
            OldOnUpdateValidateBehaviour = false; // Breaking change, but felt to better practice
            system.debug('OldOnUpdateValidateBehaviour----'+OldOnUpdateValidateBehaviour);
        }
        public Configuration enableTriggerState(){
            system.debug('Configuration....enableTriggerState');
            TriggerStateEnabled = true;
            return this;    
        }
        public Configuration disableTriggerState(){
            system.debug('Configuration....disableTriggerState');
            TriggerStateEnabled = false;
            return this;    
        }
        public Configuration enforceTriggerCRUDSecurity(){
            system.debug('Configuration....enforceTriggerCRUDSecurity');
            EnforcingTriggerCRUDSecurity = true;
            return this;
        }
        public Configuration disableTriggerCRUDSecurity(){
            system.debug('Configuration....disableTriggerCRUDSecurity');
            EnforcingTriggerCRUDSecurity = false;
            return this;
        }
        public Configuration enableOldOnUpdateValidateBehaviour() {
            system.debug('Configuration....enableOldOnUpdateValidateBehaviour');
            OldOnUpdateValidateBehaviour = true;
            return this;
        }
        public Configuration disableOldOnUpdateValidateBehaviour(){
            system.debug('Configuration....disableOldOnUpdateValidateBehaviour');
            OldOnUpdateValidateBehaviour = false;
            return this;
        }
    }
    public class DomainException extends Exception{
    }
    public String error(String message, SObject record){
        system.debug('error...');
        system.debug('message---'+message);
        system.debug('record---'+record);
        return Errors.error(this, message, record); 
    }
    public String error(String message, SObject record, SObjectField field){
        system.debug('error2---');
        system.debug('message---'+message);
        system.debug('record---'+record);
        system.debug('field---'+field);
        return Errors.error(this, message, record, field);  
    }
    public class ErrorFactory{
       
        private List<Error> errorList = new List<Error>(); 
        private ErrorFactory(){ 
        }
        public String error(String message, SObject record){
             system.debug('ErrorFactory---error');
            return error(null, message, record);    
        }
        private String error(jbsf_SObjectDomain domain, String message, SObject record){
            ObjectError objectError = new ObjectError();
            objectError.domain = domain;
            objectError.message = message;
            objectError.record = record;
            errorList.add(objectError);
            system.debug('ErrorFactory---error---domain'+domain);
            system.debug('ErrorFactory---error---message'+message);
            system.debug('ErrorFactory---error---record'+record);
            system.debug('errorList'+errorList);
            return message; 
        }
        public String error(String message, SObject record, SObjectField field){
            system.debug('error----message'+message);
            system.debug('error----record'+record);
            system.debug('error----field'+field);
            return error(null, message, record, field); 
        }
        private String error(jbsf_SObjectDomain domain, String message, SObject record, SObjectField field){
            FieldError fieldError = new FieldError();
            fieldError.domain = domain;
            fieldError.message = message;
            fieldError.record = record;
            fieldError.field = field;
            errorList.add(fieldError);
            system.debug('error....domain'+domain);
            system.debug('error....message'+message);
            system.debug('error....record'+record);
            system.debug('error....field'+field);
            system.debug('error....errorList'+errorList);
            return message; 
        }     
        public List<Error> getAll(){ 
            system.debug('getAll...return errorList.clone()');
            return errorList.clone();
        }
        public void clearAll(){
            system.debug('clearAll...errorList.clear()');
            errorList.clear();
        }                   
    }
    public virtual class FieldError extends ObjectError{
        public SObjectField field;
        public FieldError(){       
        }
    } 
    public virtual class ObjectError extends Error{
        public SObject record;
        public ObjectError(){
        }
    }
    public abstract class Error{
        public String message;
        public jbsf_SObjectDomain domain;
    }
    public class TestFactory{
        public MockDatabase Database = new MockDatabase();
        private TestFactory() {       
        }
    }
    public class MockDatabase{
        private Boolean isInsert = false;
        private Boolean isUpdate = false;
        private Boolean isDelete = false;
        private Boolean isUndelete = false;
        private List<SObject> records = new List<SObject>();
        private Map<Id, SObject> oldRecords = new Map<Id, SObject>();
        private MockDatabase(){   
        }  
        private void testTriggerHandler(Type domainClass){
            system.debug('testTriggerHandler...........');
            triggerHandler(domainClass, true, false, isInsert, isUpdate, isDelete, isUndelete, records, oldRecords);
            triggerHandler(domainClass, false, true, isInsert, isUpdate, isDelete, isUndelete, records, oldRecords);
        }
        public void onInsert(List<SObject> records){
            system.debug('onInsert...........');
            system.debug('records--------'+records);
            this.isInsert = true;
            this.isUpdate = false;
            this.isDelete = false;
            this.isUndelete = false;
            this.records = records;
        }
        public void onUpdate(List<SObject> records, Map<Id, SObject> oldRecords){
            system.debug('onUpdate...........');
            system.debug('records--------'+records);
            system.debug('oldRecords--------'+oldRecords);
            this.isInsert = false;
            this.isUpdate = true;
            this.isDelete = false;
            this.records = records;
            this.isUndelete = false;
            this.oldRecords = oldRecords;
        }
        public void onDelete(Map<Id, SObject> records){
            system.debug('onDelete...........');
            system.debug('records--------'+records);
            this.isInsert = false;
            this.isUpdate = false;
            this.isDelete = true;
            this.isUndelete = false;
            this.oldRecords = records;
        }
        public void onUndelete(List<SObject> records){
            system.debug('onUndelete...........');
            system.debug('records--------'+records);
            this.isInsert = false;
            this.isUpdate = false;
            this.isDelete = false;
            this.isUndelete = true;
            this.records = records;
        }
        public Boolean hasRecords(){
            system.debug('hasRecords...........');
            system.debug('records--------'+records);
            system.debug('oldRecords--------'+oldRecords);
            return records!=null && records.size()>0 || oldRecords!=null && oldRecords.size()>0;
        }
    }
    public with sharing class TestSObjectDomain extends jbsf_SObjectDomain{
        private String someState;
        public TestSObjectDomain(List<Opportunity> sObjectList){
            super(sObjectList);
            system.debug('TestSObjectDomain--');
        }
        public TestSObjectDomain(List<Opportunity> sObjectList, SObjectType sObjectType){ 
            super(sObjectList, sObjectType);
            system.debug('TestSObjectDomain--');
        }           
        public override void onApplyDefaults(){
            system.debug('override----onApplyDefaults--');
            super.onApplyDefaults();
            for(Opportunity opportunity : (List<Opportunity>) Records){
                opportunity.CloseDate = System.today().addDays(30);                     
            }
        }
        public override void onValidate()   {
            system.debug('override----onValidate--');
            super.onValidate();
            for(Opportunity opp : (List<Opportunity>) Records){
                if(opp.Type!=null && opp.Type.startsWith('Existing') && opp.AccountId == null){
                    opp.AccountId.addError( error('You must provide an Account for Opportunities for existing Customers.', opp, Opportunity.AccountId) );                   
                }           
            }       
        }
        
        public override void onValidate(Map<Id,SObject> existingRecords)
        {
            system.debug('override----onValidate--existingRecords---'+existingRecords);
            super.onValidate(existingRecords);
            for(Opportunity opp : (List<Opportunity>) Records){
                Opportunity existingOpp = (Opportunity) existingRecords.get(opp.Id);
                system.debug('override----onValidate--opp---'+opp);
                if(opp.Type != existingOpp.Type){
                    opp.Type.addError( error('You cannot change the Opportunity type once it has been created.', opp, Opportunity.Type) );
                }
            }
        }
        
        public override void onBeforeDelete(){
            super.onBeforeDelete();
            system.debug('override----onBeforeDelete--');
            system.debug('override----onBeforeDelete--');
            for(Opportunity opp : (List<Opportunity>) Records){
                 system.debug('override----onBeforeDelete--opp---'+opp);
                opp.addError( error('You cannot delete this Opportunity.', opp) );
            }           
        }
        
        public override void onAfterUndelete(){
            super.onAfterUndelete();
            system.debug('override----onAfterUndelete--');
        }
        public override void onBeforeInsert()
        {
            system.debug('override----onBeforeInsert--');
            someState = 'This should not survice the trigger after phase';
        }
        public override void onAfterInsert()
        {
            system.debug('override----onAfterInsert--');
            System.assertEquals(null, someState);
        }
    }  
    public class TestSObjectDomainConstructor implements jbsf_SObjectDomain.IConstructable{
        public jbsf_SObjectDomain construct(List<SObject> sObjectList)
        {
            system.debug('TestSObjectDomainConstructor---construct--'+sObjectList);
            return new TestSObjectDomain(sObjectList);
        }
    }
    
    public with sharing class TestSObjectStatefulDomain 
        extends jbsf_SObjectDomain 
    {
        public String someState;
        public TestSObjectStatefulDomain(List<Opportunity> sObjectList) {
            super(sObjectList);
            system.debug('TestSObjectStatefulDomain---sObjectList'+sObjectList);
            Configuration.enableTriggerState();         
        }        
        public override void onBeforeInsert() {
            system.debug('override---onBeforeInsert');
            System.assertEquals(null, someState);         
            List<Opportunity> newOpps = new List<Opportunity>();
            for(Opportunity opp : (List<Opportunity>) Records){
                someState = 'Error on Record ' + opp.Name;
                if(opp.Name.equals('Test Recursive 1'))
                    newOpps.add(new Opportunity ( Name = 'Test Recursive 2', Type = 'Existing Account' ));
            }                
            if(newOpps.size()>0){
                jbsf_SObjectDomain.Test.Database.onInsert(newOpps);        
                jbsf_SObjectDomain.triggerHandler(jbsf_SObjectDomain.TestSObjectStatefulDomainConstructor.class);
            }                               
        }
        public override void onAfterInsert() 
        { 
            system.debug('override---onAfterInsert');
            // Use the state set in the before insert (since this is a stateful domain class)
            if(someState!=null)
                for(Opportunity opp : (List<Opportunity>) Records)
                    opp.addError(error(someState, opp));
        }       
    }
    public class TestSObjectStatefulDomainConstructor implements jbsf_SObjectDomain.IConstructable
    {
        public jbsf_SObjectDomain construct(List<SObject> sObjectList)
        {
            system.debug('TestSObjectStatefulDomainConstructor---construct'+sObjectList);
            return new TestSObjectStatefulDomain(sObjectList);
        }               
    }
    public with sharing class TestSObjectOnValidateBehaviour 
        extends jbsf_SObjectDomain {
        public TestSObjectOnValidateBehaviour(List<Opportunity> sObjectList)
        {
            super(sObjectList);
            system.debug('TestSObjectOnValidateBehaviour---');
            system.debug('TestSObjectOnValidateBehaviour---sObjectList'+sObjectList);
            if(sObjectList[0].Name == 'Test Enable Old Behaviour')
                Configuration.enableOldOnUpdateValidateBehaviour();
        }
        
        public override void onValidate() 
        {
            system.debug('onValidate---');
            throw new DomainException('onValidate called');
        }
    }
    public class TestSObjectOnValidateBehaviourConstructor implements jbsf_SObjectDomain.IConstructable
    {
        public jbsf_SObjectDomain construct(List<SObject> sObjectList)
        {
            system.debug('TestSObjectOnValidateBehaviourConstructor---construct'+sObjectList);
            return new TestSObjectOnValidateBehaviour(sObjectList);
        }               
    }
    public with sharing class TestSObjectChangedRecords 
        extends jbsf_SObjectDomain 
    {
        public TestSObjectChangedRecords(List<Opportunity> sObjectList)
        {
            super(sObjectList);
            system.debug('TestSObjectChangedRecords---TestSObjectChangedRecords'+sObjectList);
        }
    }
    public class TestSObjectChangedRecordsConstructor implements jbsf_SObjectDomain.IConstructable
    {
        public jbsf_SObjectDomain construct(List<SObject> sObjectList)
        {
            return new TestSObjectChangedRecords(sObjectList);
        }               
    } 
    public with sharing class TestSObjectDisableBehaviour 
        extends jbsf_SObjectDomain 
    {
        public TestSObjectDisableBehaviour(List<Opportunity> sObjectList)
        {
            super(sObjectList);
        }

        public override void onAfterInsert() {
            system.debug('onAfterInsert---');
            throw new DomainException('onAfterInsert called');
        }

        public override void onBeforeInsert() {
            system.debug('DomainException---');
            throw new DomainException('onBeforeInsert called');
        }

        public override void onAfterUpdate(map<id, SObject> existing) {
            system.debug('onAfterUpdate---');
            throw new DomainException('onAfterUpdate called');
        }

        public override void onBeforeUpdate(map<id, SObject> existing) {
            system.debug('onBeforeUpdate---');
            throw new DomainException('onBeforeUpdate called');
        }

        public override void onAfterDelete() {
            system.debug('onAfterDelete---');
            throw new DomainException('onAfterDelete called');
        }

        public override void onBeforeDelete() {
            system.debug('onBeforeDelete---');
            throw new DomainException('onBeforeDelete called');
        }

        public override void onAfterUndelete() {
            system.debug('onAfterUndelete---');
            throw new DomainException('onAfterUndelete called');
        }
    }
    public class TestSObjectDisableBehaviourConstructor implements jbsf_SObjectDomain.IConstructable
    {
        public jbsf_SObjectDomain construct(List<SObject> sObjectList)
        {
            system.debug('TestSObjectDisableBehaviourConstructor----jbsf_SObjectDomain');
            return new TestSObjectDisableBehaviour(sObjectList);
        }
    }
}