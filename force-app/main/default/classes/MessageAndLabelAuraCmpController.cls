public class MessageAndLabelAuraCmpController {
    @AuraEnabled(cacheable=true)
    public static Map<String, Message_And_Label__c> getMessageAndLabel(){
        system.debug('Customer Account using System.label-->'+System.Label.Customer_Account);
        system.debug('Customer Account using Label-->'+Label.Customer_Account);
        return Message_And_Label__c.getAll();
    }
}